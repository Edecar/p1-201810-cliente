package Test;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase{
private Queue<Integer> fila;
	
	
	public void test01(){
		fila= new Queue();
		
		fila.enqueue(1);
		fila.enqueue(2);
		fila.enqueue(3);
		fila.enqueue(4);
		fila.enqueue(5);
		
		String s = fila.toString();
		assertEquals("1 2 3 4 5 ", s);
		assertEquals(5,fila.size());
		
	}
	
	public void test02(){
		fila= new Queue();
		
		fila.enqueue(1);
		int n= fila.peek();
		assertEquals(1, fila.peek().intValue());
		fila.enqueue(2);
		assertEquals(1, fila.peek().intValue());
		fila.enqueue(3);
		assertEquals(1, fila.peek().intValue());
		fila.enqueue(4);
		assertEquals(1, fila.peek().intValue());
		fila.enqueue(5);
		assertEquals(1, fila.peek().intValue());
		fila.dequeue();
		assertEquals(2, fila.peek().intValue());
		fila.dequeue();
		assertEquals(3, fila.peek().intValue());
		
		String s = fila.toString();
		assertEquals("3 4 5 ", s);
		assertEquals(3, fila.size());
		
		
		
	}
	
	public void test03(){
		
		fila= new Queue();
		assertEquals(0, fila.size());
		assertEquals(true, fila.isEmpty());
		
		fila.enqueue(1);
		fila.enqueue(2);
		fila.enqueue(3);
		fila.enqueue(4);
		fila.enqueue(5);
		
		assertEquals(5, fila.size());
		assertEquals(false, fila.isEmpty());
		
	}

}
