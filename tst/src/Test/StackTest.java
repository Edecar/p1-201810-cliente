package Test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest<T> extends TestCase{
private Stack<T> pila;
	
	public void test01(){
		pila= new Stack();
		
		assertEquals(0, pila.size());
		
		pila.push((T) "a");
		pila.push((T) "e");
		pila.push((T) "i");
		pila.push((T) "o");
		pila.push((T) "u");
		String s= pila.toString();
		assertEquals("u o i e a ", s);
		assertEquals(5, pila.size());
		
		
	}
	public void test02(){
		pila= new Stack();
		pila.push((T) "a");
		pila.push((T) "e");
		pila.pop();
		pila.push((T) "i");
		pila.push((T) "o");
		pila.pop();
		pila.push((T) "u");
		
		String s= pila.toString();
		assertEquals("u i a ", s);
		assertEquals(3, pila.size());
		assertEquals(false, pila.isEmpty());
	}
	
	public void test03(){
		pila= new Stack();
		
		assertEquals(true, pila.isEmpty());
		
		pila.push((T) "a");
		assertEquals("a", pila.peek());
		pila.push((T) "e");
		assertEquals("e", pila.peek());
		pila.push((T) "i");
		assertEquals("i", pila.peek());
		pila.push((T) "o");
		assertEquals("o", pila.peek());
		pila.push((T) "u");
		assertEquals("u", pila.peek());
	}
	
	

}
