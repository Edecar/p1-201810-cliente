package Test;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class DoubleLinkedListTest extends TestCase{
	private DoubleLinkedList doublyLinkedList;
	
	
	public void test01(){
		doublyLinkedList= new  DoubleLinkedList();
		doublyLinkedList.add(10);
		doublyLinkedList.add(2);
		doublyLinkedList.add(23);
		doublyLinkedList.add(15);
		String s= doublyLinkedList.toString();
		
		assertEquals(Integer.valueOf(4), doublyLinkedList.getSize());
		assertEquals("[15 23 2 10]", s);
	}
	
	public void test02(){
		doublyLinkedList= new  DoubleLinkedList();
		doublyLinkedList.AddAtEnd(10);
		doublyLinkedList.AddAtEnd(2);
		doublyLinkedList.AddAtEnd(23);
		doublyLinkedList.AddAtEnd(15);
		String s= doublyLinkedList.toString();
		
		assertEquals(Integer.valueOf(4), doublyLinkedList.getSize());
		assertEquals("[10 2 23 15]", s);
		
	}
	
	public void test03(){
		doublyLinkedList= new  DoubleLinkedList();
		doublyLinkedList.AddAtEnd(10);
		doublyLinkedList.AddAtEnd(2);
		doublyLinkedList.AddAtEnd(23);
		doublyLinkedList.AddAtEnd(15);
		doublyLinkedList.addAtk(34, 3);
		doublyLinkedList.addAtk(33, 5);
		doublyLinkedList.addAtk(1, 1);
		doublyLinkedList.addAtk(99, 7);
		doublyLinkedList.addAtk("hola", 10);
		String s= doublyLinkedList.toString();
		
		assertEquals(Integer.valueOf(9), doublyLinkedList.getSize());
		assertEquals("[1 10 2 34 23 15 33 99 hola]", s);
	}
	
	public void test04(){
		doublyLinkedList= new  DoubleLinkedList();
		doublyLinkedList.add(10);
		doublyLinkedList.add(2);
		doublyLinkedList.add(23);
		doublyLinkedList.add(15);
		
		
		assertEquals(Integer.valueOf(15), doublyLinkedList.getElement(1));
		assertEquals(Integer.valueOf(23), doublyLinkedList.getElement(2));
		assertEquals(Integer.valueOf(2), doublyLinkedList.getElement(3));
		assertEquals(Integer.valueOf(10), doublyLinkedList.getElement(4));
	}
	
	public void test05(){
		doublyLinkedList = new DoubleLinkedList();
		doublyLinkedList.add(10);
		doublyLinkedList.add(2);
		doublyLinkedList.add(23);
		doublyLinkedList.add(15);
		doublyLinkedList.delete(10);
		
		String s= doublyLinkedList.toString();
		
		assertEquals("[15 23 2]", s );
		
	}
	
	public void test06(){
		doublyLinkedList = new DoubleLinkedList();
		doublyLinkedList.add(10);
		doublyLinkedList.add(2);
		doublyLinkedList.add(23);
		doublyLinkedList.add(15);
		doublyLinkedList.deleteAtk(3);
		
		String s = doublyLinkedList.toString();
		
		assertEquals("[15 23 10]", s);
		
	}
	public void test07(){
		doublyLinkedList = new DoubleLinkedList();
		doublyLinkedList.add("aaa");
		doublyLinkedList.add("eee");
		doublyLinkedList.add("iii");
		doublyLinkedList.add("ooo");
		
		assertEquals("aaa", doublyLinkedList.getElementByT("aaa"));
		assertEquals("eee", doublyLinkedList.getElementByT("eee"));
		assertEquals("iii", doublyLinkedList.getElementByT("iii"));
		assertEquals("ooo", doublyLinkedList.getElementByT("ooo"));

		String s = doublyLinkedList.toString();
		
		assertEquals("[ooo iii eee aaa]", s);
		
	}
	
	


}
