package model.data_structures;

public class Node<E> {
	
	private E elemento;
	private Node<E> siguiente;
	private Node<E> anterior;
	
	public Node(){
		siguiente= null;
		anterior= null;
	}
	public void cambiarSiguiente(Node pElemento){
		siguiente= pElemento;
	}
	public void cambiarAnterior(Node pElemento){
		anterior= pElemento;
	}
	public Node darAnterior(){
		return anterior;
	}
	public Node<E> darSiguiente(){
		return siguiente;
	}
	public void agregarElemento(E pElemento){
		elemento=pElemento;
	}
	public E darElemento(){
		return elemento;
	}
}
